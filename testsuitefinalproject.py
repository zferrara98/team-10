import unittest

class test_NBAPredictor(unittest.testcase):

    def SetUp(self):
        bring in dataset
        create necessary variables
        
    def test_init(self):
	    self.assertIsInstance(self.NBAPredictor.team1boards, float)
	    self.assertIsInstance(self.NBAPredictor.team1points, float)
	    self.assertIsInstance(self.NBAPredictor.team1assists, float)
	    self.assertIsInstance(self.NBAPredictor.team1score, int)
	    self.assertIsInstance(self.NBAPredictor.team2points, float)
	    self.assertIsInstance(self.NBAPredictor.team2assists, float)
	    self.assertIsInstance(self.NBAPredictor.team2boards, float)
	    self.assertIsInstance(self.NBAPredictor.team2score, int)
	    self.assertIsInstance(self.NBAPredictor.eighteen_stats, pd.DataFrame)
    
        
    def input_teams(self):
        self.assertIsInstance((self.inactive),(str))
        if self.assertIsInstance is False:
            raise ValueError
        
    def team_stat_effects(self):
        self.assertIsInstance(o_adv1, int)
        self.assertIsInstance(o_adv2, int)
        
    def adv_team_stat_effects(self):
        self.assertIsInstance(team1TOadv, int)
        self.assertIsInstance(team2TOadv, int)
    
    def input_inactives(self):
        self.assertIsInstance(team1score, int)
        self.assertIsInstance(team2score, int)
        
    def test_predict_score():
        self.assertIsInstance(team1score, int)
        self.assertIsInstance(team2score, int)
        

    def test_winning_percentage():
        self.assertIsInstance(team1winp, float)
        self.assertIsInstance(team2winp, float)
        self.assertTrue(winning_percentage() > 0 and winning_percentage() < 100)
        
def run_prediction():
